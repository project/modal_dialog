<?php

namespace Drupal\modal_dialog\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for modal_dialog routes.
 */
class ModalDialogController extends ControllerBase {

  /**
   *
   */
  public function openMyModal() {
    $response = new AjaxResponse();
    $modal_form = $this->formBuilder()->getForm('Drupal\modal_dialog\Form\ModalDialogForm');
    $options = [
      'width' => '75%',
      'closeOnEscape' => TRUE,
    ];
    $response->addCommand(new OpenModalDialogCommand('My Modal', $modal_form, $options));
    return $response;
  }

}
