<?php

namespace Drupal\modal_dialog\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class ModalDialogForm extends FormBase {

  /**
   *
   */
  public function getFormId() {
    return 'modal_dialog';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['modal'] = [
      '#title' => $this->t('This is my modal.'),
      '#description' => $this->t("We'ĺl send updates to the email address your provide."),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('OK'),
    ];

    return $form;
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addMessage('Hello World');
    $form_state->setRedirect('<front>');
  }

}
