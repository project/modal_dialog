<?php

namespace Drupal\modal_dialog\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Modal Dialog settings for this site.
 */
class ModalDialogSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'modal_dialog_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'modal_dialog.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('modal_dialog.settings');

    $form['modal_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Global Modal Settings'),
      '#open' => TRUE,
    ];
    // allowed_tags:
    $form['modal_settings']['html_tags'] = [
      '#type' => 'details',
      '#title' => $this->t('HTML tags'),
      '#open' => TRUE,
    ];

    $form['modal_settings']['html_tags']['allowed_tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed HTML tags'),
      '#description' => $this->t("List of HTML tags that can be used in your modal, separeted by a comma(,)."),
      '#default_value' => $config->get('allowed_tags') ?? "h1,h2,a,b,big,code,del,em,i,ins,pre,q,small,span,strong,sub,sup,tt,ol,ul,li,p,br,img",
    ];
    // clear_caches_on_modal_save.
    $form['modal_settings']['cache_option'] = [
      '#type' => 'details',
      '#title' => $this->t('Cache clear on save'),
      '#open' => TRUE,
    ];

    $form['modal_settings']['cache_option']['clear_caches_on_modal_save'] = [
      '#title' => $this->t("Clear caches on Modal save"),
      '#type' => 'checkbox',
      '#default_value' => $config->get('clear_caches_on_modal_save'),
    ];
    // default_cookie_expiration.
    $form['modal_settings']['cookie_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Cookies settings'),
      '#open' => TRUE,
    ];
    $form['modal_settings']['cookie_settings']['default_cookie_expiration'] = [
      '#type' => 'number',
      '#title' => $this->t('Default Cookie Expiration Setting'),
      '#description' => $this->t("Enter value in days. Enter 0 to expire at end of session.
            <br> Default is 10000 (never show again)."),
      '#default_value' => $config->get('default_cookie_expiration') ?? 10000,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

}
