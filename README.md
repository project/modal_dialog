# Modal Dialog

## Table of Contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainer


## Introduction

This module Provides a configurable modal pages using Drupal Dialog API.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see:
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configure your messages in:

Administration » Structure » Modal Dialog

1. Set the Title of modal.
1. Set the Text of modal (Body).
1. Set pages to show the Modal.


## Maintainer

- Marcus Souza (marcusvsouza) - https://www.drupal.org/u/marcusvsouza
