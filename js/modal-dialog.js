(function ($, Drupal) {
    'use strict';
    Drupal.behaviors.modal_dialog = {
      attach: function (context) {
        $(context).find('body')
          .once('modal_dialog')
          .each(function () {
            var ajaxSettings = {
              url: '/mymodal'
              //$modals
            };
            var myAjaxObject = Drupal.ajax(ajaxSettings);
            myAjaxObject.execute();
          });
      }
    };

  })(jQuery, Drupal);
